package pl.edu.agh.qa.library;

import java.util.StringJoiner;

public class ItemStatistics {
    private Integer availability;
    private Integer totalNumber;

    public ItemStatistics(int availability, int totalNumber) {
        this.availability = availability;
        this.totalNumber = totalNumber;
    }

    public Integer getAvailability() {
        return availability;
    }

    public void setAvailability(Integer availability) {
        this.availability = availability;
    }

    public Integer getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(Integer totalNumber) {
        this.totalNumber = totalNumber;
    }

    public void incrementAvailability(int availability) {
        this.availability = this.availability + availability;
    }

    public void decrementAvailability() {
        availability--;
    }

    public void incrementTotalNumber(int totalNumber) {
        this.totalNumber = this.totalNumber + totalNumber;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ItemStatistics.class.getSimpleName() + "[", "]")
                .add("availability=" + availability)
                .add("totalNumber=" + totalNumber)
                .toString();
    }
}
