package pl.edu.agh.qa.library;


import pl.edu.agh.qa.item.Book;
import pl.edu.agh.qa.item.Item;
import pl.edu.agh.qa.item.Magazine;
import pl.edu.agh.qa.users.User;

import java.io.*;
import java.util.*;

public class Library {

    public static final String COMMA_DELIMITER = ";";
    public static final String BOOK = "B";
    public static final String MAGAZINE = "M";

    private List<User> usersList;
    private Map<Book, ItemStatistics> booksWithAvailabilityMap;
    private Map<Magazine, ItemStatistics> magazinesWithAvailabilityMap;
    private Map<User, List<Item>> numberOfBorrowedItemsPerUser;

    public Library() {
        usersList = new ArrayList<>();
        booksWithAvailabilityMap = new HashMap<>();
        magazinesWithAvailabilityMap = new HashMap<>();
        numberOfBorrowedItemsPerUser = new HashMap<>();
    }

    public List<User> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<User> usersList) {
        this.usersList = usersList;
    }

    public Map<Book, ItemStatistics> getBooksWithAvailabilityMap() {
        return booksWithAvailabilityMap;
    }

    public void setBooksWithAvailabilityMap(Map<Book, ItemStatistics> booksWithAvailabilityMap) {
        this.booksWithAvailabilityMap = booksWithAvailabilityMap;
    }

    public Map<Magazine, ItemStatistics> getMagazinesWithAvailabilityMap() {
        return magazinesWithAvailabilityMap;
    }

    public void setMagazinesWithAvailabilityMap(Map<Magazine, ItemStatistics> magazinesWithAvailabilityMap) {
        this.magazinesWithAvailabilityMap = magazinesWithAvailabilityMap;
    }

    public Map<User, List<Item>> getNumberOfBorrowedItemsPerUser() {
        return numberOfBorrowedItemsPerUser;
    }

    public void setNumberOfBorrowedItemsPerUser(Map<User, List<Item>> numberOfBorrowedItemsPerUser) {
        this.numberOfBorrowedItemsPerUser = numberOfBorrowedItemsPerUser;
    }

    public void addUserToLibrary(User... users) {
        for (User user : users) {
            long nextNumber = NumberGenerator.getNextNumber();
            user.setCardNumber(nextNumber);
            usersList.add(user);
            numberOfBorrowedItemsPerUser.put(user, new ArrayList<>());
        }
    }

    public void addItemToLibrary(Item... items) {
        for (Item item : items) {
            addItem(item, 1);
        }
    }

    private void addItem(Item item, int totalNumber) {
        if (item instanceof Book) {
            Book book = (Book) item;
            if (booksWithAvailabilityMap.containsKey(book)) {
                ItemStatistics itemStatistics = booksWithAvailabilityMap.get(book);
                itemStatistics.incrementAvailability(totalNumber);
                itemStatistics.incrementTotalNumber(totalNumber);
                booksWithAvailabilityMap.put(book, itemStatistics);
            } else {
                booksWithAvailabilityMap.put(book, new ItemStatistics(totalNumber, totalNumber));
            }
        } else if (item instanceof Magazine) {
            Magazine magazine = (Magazine) item;
            if (magazinesWithAvailabilityMap.containsKey(magazine)) {
                ItemStatistics itemStatistics = magazinesWithAvailabilityMap.get(magazine);
                itemStatistics.incrementAvailability(totalNumber);
                itemStatistics.incrementTotalNumber(totalNumber);
                magazinesWithAvailabilityMap.put(magazine, itemStatistics);
            } else {
                magazinesWithAvailabilityMap.put(magazine, new ItemStatistics(totalNumber, totalNumber));
            }
        }
    }

    public boolean rentItemToUser(Item item, User user) {
        if (item instanceof Book) {
            ItemStatistics bookStatistics = booksWithAvailabilityMap.get(item);
            if (canRentItem(user) && bookStatistics.getAvailability() > 0) {
                bookStatistics.decrementAvailability();
                List<Item> itemList = numberOfBorrowedItemsPerUser.get(user);
                itemList.add(item);
                return true;
            } else {
                return false;
            }
        } else if (item instanceof Magazine) {
            ItemStatistics magazineStatistics = magazinesWithAvailabilityMap.get(item);
            if (canRentItem(user) && magazineStatistics.getAvailability() > 0) {
                magazineStatistics.decrementAvailability();
                List<Item> itemList = numberOfBorrowedItemsPerUser.get(user);
                itemList.add(item);
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    private boolean canRentItem(User user) {
        List<Item> itemList = numberOfBorrowedItemsPerUser.get(user);
        if (itemList == null || itemList.size() < user.getMaxBorrowBook()) { //NOSONAR
            return true;
        }
        return false;
    }

    public void printListOfMagazines() {
        for (Map.Entry<Magazine, ItemStatistics> entry : magazinesWithAvailabilityMap.entrySet()) {
            Magazine magazine = entry.getKey();
            ItemStatistics itemStatistics = entry.getValue();

            String magazineTitle = magazine.getTitle();
            String magazineNumber = magazine.getMagazineNumber();
            Integer totalNumber = itemStatistics.getTotalNumber();
            Integer availability = itemStatistics.getAvailability();
            System.out.println(magazineTitle + ";" + magazineNumber + ";" + totalNumber + ";" + availability);
        }
    }

    public void printListOfBooks() {
        for (Map.Entry<Book, ItemStatistics> entry : booksWithAvailabilityMap.entrySet()) {
            Book book = entry.getKey();
            ItemStatistics itemStatistics = entry.getValue();

            String bookTitle = book.getTitle();
            String bookAuthor = book.getAuthor();
            Integer totalNumber = itemStatistics.getTotalNumber();
            Integer availability = itemStatistics.getAvailability();
            System.out.println(bookTitle + ";" + bookAuthor + ";" + totalNumber + ";" + availability);
        }
    }

    public void printListOfUsers() {
        for (User libraryUser : usersList) {
            String myUserString = libraryUser.myToString();
            System.out.println(myUserString);
        }
    }

    public void importItemsFromFile(String csvFile) {
        List<List<String>> listsOfEntries = readRecordsFromCSVFile(csvFile);
        for (List<String> item : listsOfEntries) {
            String title = item.get(0);
            String authorOrNumber = item.get(1);
            Integer totalNumber = Integer.valueOf(item.get(2));
            String type = item.get(3);
            Item newItem;
            if (BOOK.equals(type)) {
                newItem = new Book(authorOrNumber, title);
                addItem(newItem, totalNumber);
            } else if (MAGAZINE.equals(type)) {
                newItem = new Magazine(authorOrNumber, title);
                addItem(newItem, totalNumber);
            }
        }
    }

    private List<List<String>> readRecordsFromCSVFile(String csvFile) {
        List<List<String>> records = new ArrayList<>();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(csvFile);
        Reader reader = new InputStreamReader(inputStream);
        try (BufferedReader br = new BufferedReader(reader)) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                List<String> row = Arrays.asList(values);
                records.add(row);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException("Wrong file");
        }
        return records;
    }

    public void exportUsersWithItemsToFile(String csvFile) throws IOException {
        List<String> userWithItems = createUserWithItems();
        saveUsersToFile(csvFile, userWithItems);
    }

    private List<String> createUserWithItems() {
        List<String> userWithItems = new ArrayList<>();
        for (Map.Entry<User, List<Item>> entry : numberOfBorrowedItemsPerUser.entrySet()) {
            User user = entry.getKey();
            long userCardNumber = user.getCardNumber();
            List<Item> itemList = entry.getValue();
            if (!itemList.isEmpty()) {
                StringBuilder st = new StringBuilder();
                for (Item item : itemList) {
                    if (item instanceof Book) {
                        Book book = (Book) item;
                        String bookAuthor = book.getAuthor();
                        String bookTitle = book.getTitle();
                        String bookItem = bookTitle + "-" + bookAuthor + "; ";
                        st.append(bookItem);
                    } else if (item instanceof Magazine) {
                        Magazine magazine = (Magazine) item;
                        String magazineNumber = magazine.getMagazineNumber();
                        String magazineTitle = magazine.getTitle();
                        String magazineItem = magazineTitle + "-" + magazineNumber + "; ";
                        st.append(magazineItem);
                    }
                }
                String allItems = st.substring(0, st.length() - 2);
                String fullUserItem = userCardNumber + "[" + allItems + "]";
                userWithItems.add(fullUserItem);
            }
        }
        return userWithItems;
    }

    private void saveUsersToFile(String csvFile, List<String> userWithItems) throws IOException {
        try (FileWriter writer = new FileWriter(csvFile)) {
            for (String str : userWithItems) {
                writer.write(str + System.lineSeparator());
            }
        }
    }
}






