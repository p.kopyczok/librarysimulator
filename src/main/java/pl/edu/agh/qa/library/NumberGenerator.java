package pl.edu.agh.qa.library;

public abstract class NumberGenerator {

    private NumberGenerator() {
    }

    private static long number;

    public static long getNextNumber() {
        return ++number;
    }

}
