package pl.edu.agh.qa.users;

import java.util.Objects;
import java.util.StringJoiner;

public abstract class User {

    private String name;
    private String surname;
    private long cardNumber;
    private int maxBorrowBook;

    protected User(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getMaxBorrowBook() {
        return maxBorrowBook;
    }

    public void setMaxBorrowBook(int maxBorrowBook) {
        this.maxBorrowBook = maxBorrowBook;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return cardNumber == user.cardNumber &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, cardNumber);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("surname='" + surname + "'")
                .add("cardNumber=" + cardNumber)
                .toString();
    }

    public String myToString() {
        return new StringJoiner(";", "", "")
                .add(name)
                .add(surname)
                .add(String.valueOf(cardNumber))
                .add(getAcronym())
                .toString();
    }

    abstract String getAcronym();

}

