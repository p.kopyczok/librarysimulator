package pl.edu.agh.qa.users;

public class Student extends User {

    private static final int STUDENT_MAX_BORROW_BOOK = 4;

    public Student(String firstName, String lastName) {
        super(firstName, lastName);
        setMaxBorrowBook(STUDENT_MAX_BORROW_BOOK);
    }

    @Override
    String getAcronym() {
        return "S";
    }

}
