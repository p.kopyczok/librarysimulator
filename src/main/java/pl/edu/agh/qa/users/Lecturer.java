package pl.edu.agh.qa.users;

public class Lecturer extends User {

    private static final int LECTURER_MAX_BORROW_BOOK = 10;

    public Lecturer(String firstName, String lastName) {
        super(firstName, lastName);
        setMaxBorrowBook(LECTURER_MAX_BORROW_BOOK);
    }

    @Override
    String getAcronym() {
        return "L";
    }


}
