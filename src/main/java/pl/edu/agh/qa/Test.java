package pl.edu.agh.qa;

import pl.edu.agh.qa.item.Book;
import pl.edu.agh.qa.item.Item;
import pl.edu.agh.qa.item.Magazine;
import pl.edu.agh.qa.library.Library;
import pl.edu.agh.qa.users.Lecturer;
import pl.edu.agh.qa.users.Student;
import pl.edu.agh.qa.users.User;

import java.io.IOException;

public class Test {

    public static void main(String[] args) throws IOException {

        Library library = new Library();
        User user = new Student("Pawel", "Kopyczok");
        User user1 = new Student("Jan", "Kowalski");
        User user2 = new Lecturer("Andrzej", "Nowakowski");
        User user3 = new Lecturer("Adam", "Adamowicz");

        library.addUserToLibrary(user, user1, user2, user3);

        library.printListOfUsers();

        Item book = new Book("Jan", "Title of the book");
        Item book2 = new Book("Jan", "Title of the book");
        Item mag = new Magazine("2002/8", "Title of the mag");
        Item mag2 = new Magazine("2010/11", "ABC");
        Item mag3 = new Magazine("2010/11", "XYZ");
        Item mag4 = new Magazine("01/2016", "National Geographic");

        library.addItemToLibrary(book, book, book, book);
        library.addItemToLibrary(book2);
        library.addItemToLibrary(book2);
        library.addItemToLibrary(book2);
        library.addItemToLibrary(book2);
        library.addItemToLibrary(mag);
        library.addItemToLibrary(mag);
        library.addItemToLibrary(mag);
        library.addItemToLibrary(mag);
        library.addItemToLibrary(mag);
        library.addItemToLibrary(mag2);
        library.addItemToLibrary(mag2);
        library.addItemToLibrary(mag2);
        library.addItemToLibrary(mag2);
        library.addItemToLibrary(mag3);
        library.addItemToLibrary(mag3);
        library.addItemToLibrary(mag3);
        library.addItemToLibrary(mag4);
        library.addItemToLibrary(mag4);
        library.addItemToLibrary(mag4);
        library.addItemToLibrary(mag4);


        library.rentItemToUser(mag, user);
        library.rentItemToUser(mag4, user);
        library.rentItemToUser(book, user1);
        library.rentItemToUser(book2, user2);
        library.rentItemToUser(mag, user3);

        library.importItemsFromFile("itemList.csv");
        library.printListOfBooks();
        library.printListOfMagazines();
        library.exportUsersWithItemsToFile("output.txt");
    }
}
