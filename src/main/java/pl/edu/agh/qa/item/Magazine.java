package pl.edu.agh.qa.item;

import java.util.Objects;
import java.util.StringJoiner;

public class Magazine extends Item {

    private String magazineNumber;

    public Magazine(String magazineNumber, String title) {
        super(title);
        this.magazineNumber = magazineNumber;
    }


    public String getMagazineNumber() {
        return magazineNumber;
    }

    public void setMagazineNumber(String magazineNumber) {
        this.magazineNumber = magazineNumber;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Magazine.class.getSimpleName() + "[", "]")
                .add("magazineNumber='" + magazineNumber + "'")
                .add("title='" + title + "'")
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Magazine)) return false;
        if (!super.equals(o)) return false;
        Magazine magazine = (Magazine) o;
        return Objects.equals(magazineNumber, magazine.magazineNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), magazineNumber);
    }
}

